// JavaScript source code

console.log("Hello World");

const firstName = "Naz", lastName = "Glang";
let age = 19;
let address = {
    city: "Cagayan de Oro City",
    province: "Misamis Oriental"
};


/* Create a function that will accept the firstName, lastName, age, and current address (city/municipality & province) and return a complete sentence in this format: 
 * "Hello! I am Naz Glang, 19 years old, and currently living in Cagayan de Oro City, Misamis Oriental." */

function personalInfo() {
    return console.log("Hello! I am " + firstName + " " + lastName + ", " + age + " years old, and currently living in " + address.city + ", " + address.province + ".");
};

personalInfo();


/* Create a conditional statement that will check the age of a person and will determine if he/she is qualified to voye. if the person's age is 18 years old and above print the message
 "You're qualified to vote!" else print "Sorry, You're too young to vote." */

function verifyAge() {
    let message;
    age = 17;
    if (age >= 18) {
        message = 'You\'re qualified to vote!';
    } else {
        message = 'Sorry, You\'re too young to vote.';
    }
    return console.log(message);
}

verifyAge();

/* Create a switch case statement that will accept the input month number from a user anf will print the number of days in that month. 
 * If the input month number is not valid display alert: "Invalid input! Please enter the month number between 1-12" */


/*function switchCase() {
    let monthNumber = parseInt(prompt("Please input the month number between 1-12"));
    let output;

    function days() {
        return new Date(2022, monthNumber, 0).getDate();
    }

    switch (monthNumber) {
        case 1: output = "Total number of days for January" + ": " + days();
            break;
        case 2: output = "Total number of days for February" + ": " + days();
            break;
        case 3: output = "Total number of days for March" + ": " + days();
            break;
        case 4: output = "Total number of days for April" + ": " + days();
            break;
        case 5: output = "Total number of days for May" + ": " + days();
            break;
        case 6: output = "Total number of days for June" + ": " + days();
            break;
        case 7: output = "Total number of days for July" + ": " + days();
            break;
        case 8: output = "Total number of days for August" + ": " + days();
            break;
        case 9: output = "Total number of days for September" + ": " + days();
            break;
        case 10: output = "Total number of days for October" + ": " + days();
            break;
        case 11: output = "Total number of days for November" + ": " + days();
            break;
        case 12: output = "Total number of days for December" + ": " + days();
            break;
        default:
            output = "Invalid Input! Please enter the month number between 1-12";
    }
    return console.log(output);
}

switchCase();*/


/* Create a function that will check if teh year inputted by a user is a leap year. Using selection control structure, return the year and statement
 * (Ex. "2016 is a leap year") if the year is a leap year. Otherwise, return a statement "Not a leap year!".
 * 
 * Note:
 * A year is a leap year if:
 * 1. The year is divisible by 400.
 * 2. The year is divisible by 4 but not by 100.*/


/*
let year = parseInt(prompt("Please input the year"));
let result;
function leapYear() {
    if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0) == true) {
        result = year + " is a leap year";
    }
    else {
        result = "Not a leap year!";
    }
    return console.log(result);
};

leapYear();
*/


/* Create a function that will accept a  number from the user anf will count down until zero. Use a looping statement to print each number in the browser console */

let number = parseInt(prompt("Please input number!"));
console.log(number);
for (number; number--;) {
    console.log(number);
};
